package org.testng;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestNGAssignment1 {
	WebDriver driver = new ChromeDriver();

	@Test
	public void enableText() {
		WebElement text = driver.findElement(By.id("headerContainer"));
		System.out.println(text.isDisplayed());
		String string = text.toString();
		String s = "Please identify";
		//System.out.println(string.equalsIgnoreCase(s));

	}

	@Test
	public void enableLogo() {
		WebElement img = driver.findElement(By.xpath("//div[@class='atLogoImg']"));
         System.out.println(img.isDisplayed());

	}

	@BeforeClass
	public void launchBrowser() {
		driver.get("https://demo.actitime.com/login.do");
		driver.manage().window().maximize();

	}

	@AfterClass
	public void quitBrowser() {
		driver.close();

	}
}
