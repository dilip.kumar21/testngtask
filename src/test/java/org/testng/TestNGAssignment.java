package com.testng;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestNGAssignment {
	WebDriver driver = new ChromeDriver();
	@Test(priority = 1)
	public void launchApplication() {
		driver.findElement(By.id("Email")).sendKeys("pdkumar241095@gmail.com");
		driver.findElement(By.id("Password")).sendKeys("Dilipkd#2124");
		driver.findElement(By.xpath("(//input[@type='submit'])[2]")).click();
		System.out.println("Successfully Launched Application");
	}

	@Test(priority = 3)
	public void purchasing() {
		driver.findElement(By.xpath("(//input[@value='Add to cart'])[1]")).click();
		driver.findElement(By.xpath("(//a[@class='ico-cart'])[1]")).click();
		driver.findElement(By.xpath("//input[@name='removefromcart']")).click();
		driver.findElement(By.id("termsofservice")).click();
		driver.findElement(By.xpath("//button[@id='checkout']")).click();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
		driver.findElement(By.xpath("(//input[@class='button-1 new-address-next-step-button'])[1]")).click();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
		driver.findElement(By.xpath("(//input[@class='button-1 new-address-next-step-button'])[2]")).click();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
		driver.findElement(By.xpath("//input[@class='button-1 shipping-method-next-step-button']")).click();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
		driver.findElement(By.xpath("//input[@class='button-1 payment-method-next-step-button']")).click();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
		driver.findElement(By.xpath("//input[@class='button-1 payment-info-next-step-button']")).click();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
		driver.findElement(By.xpath("//input[@value='Confirm']")).click();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
		driver.findElement(By.xpath("//a[text()='Click here for order details.']")).click();
		List<WebElement> orderlist = driver.findElements(By.xpath("//div[@class='page order-details-page']"));
		for (WebElement billpage : orderlist) {
			System.out.println(billpage.getText());

		}
		driver.findElement(By.xpath("(//a[text()='Log out'])[1]")).click();
		System.out.println("Successfully Logged out");
		//System.out.println("The customer will get created");
	}

	@Test(priority = 2)
	public void electronics() {
		WebElement electronics = driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Electronics')]"));
		Actions a = new Actions(driver);
		a.moveToElement(electronics).build().perform();                               
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
		a.moveToElement(driver.findElement(By.partialLinkText("Cell phones"))).click().perform();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
		System.out.println("Product Selected");
	}

	@BeforeMethod
	public void beforeCustomer() {
		System.out.println("Verifying the customer");
	}

	@AfterMethod
	public void afterustomer() {
		System.out.println("Customer Verified Succesfully : Valid Customer");
	}

	@BeforeClass
	public void beforeClass() {
	
		driver.get("https://demowebshop.tricentis.com/login");
		driver.manage().window().maximize();
		System.out.println("Start Database Connection, Launch Browser");
		
	}

	@AfterClass
	public void afterClass() {
		
		driver.close();
		System.out.println("Close Database Connection, Close Browser");
	}
}
